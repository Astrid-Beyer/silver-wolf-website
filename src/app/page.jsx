import React, { Suspense } from "react";
import ReactDOM from "react-dom/client";

import styles from "./styles/page.module.css";
import { ReactLenis } from "@studio-freight/react-lenis";
import { ParallaxProvider } from "react-scroll-parallax";
import "./globals.css";
import "./style.css";

import MainSection from "./components/MainSection";
import Aether from "./components/Aether";
import Portrait from "./components/Portrait";
import Faction from "./components/Faction";
import ScrollToTop from "./components/ScrollToTop";
import { LoadingProvider } from "./contexts/loading.context";

import WhoAmI from "./components/WhoAmI";

export default function Home() {
  return (
    <LoadingProvider>
      <main className={styles.main}>
        <ReactLenis root>
          <MainSection />
          <WhoAmI />
          <Aether />
          <ParallaxProvider>
            <Portrait />
          </ParallaxProvider>
          <Faction />
          <ScrollToTop />
        </ReactLenis>
      </main>
    </LoadingProvider>
  );
}

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<Home />);
