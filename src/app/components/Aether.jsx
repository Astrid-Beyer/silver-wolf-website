import React from "react";
import styles from "../styles/aether.module.css";

const Aether = () => {
  return (
    <section>
      <h2 id="aetherediting" className={styles.sectionTitle}>
        Aether <span style={{ display: "inline-block" }}>Editing</span>
      </h2>
      <div className={styles.paras}>
        <p className={styles.leftPara}>
          AE is a technology that can be used to tamper with the data of
          reality. I always see the universe as a massive immersive simulation
          game, as im eager to clear the stages waiting ahead.
        </p>
        <p className={styles.rightPara}>
          <span>Aether Cartridge</span> - it’s the second pair of eyes, second
          brain, and second heart of the hacker. With that cartridge, if a guy
          went to a movie premiere and bought some mixed popcorn, you'd be able
          to know the flavor of the fourth kernel he picked out of the bucket.
          It's truly amazing.
        </p>
      </div>
    </section>
  );
};

export default Aether;
