import React, { useEffect, useState, useContext } from "react";
import styles from "../styles/loadingScreen.module.css";
import { loadingContext } from "../contexts/loading.context";

import {
  MouseParallaxContainer,
  MouseParallaxChild,
} from "react-parallax-mouse";

import mask from "../../images/contour.svg";
import arrows from "../../images/arrows.svg";

const LoadScreen = (props) => {
  const [urlTxt, setUrlTxt] = useState("42LK2L");
  const { percent } = useContext(loadingContext);

  const generateTxt = () => {
    return btoa(Math.random()).slice(6, 12).toUpperCase();
  };

  const randInterval = () => {
    let rand = Math.floor(Math.random() * (700 - 200 + 1) + 200);
    setUrlTxt(generateTxt());
    setTimeout(randInterval, rand);
  };

  useEffect(() => {
    const interval = setTimeout(randInterval, 200);
    setInterval(interval);
    return () => clearTimeout(interval);
  }, []);

  if (percent >= 100) return props.children;

  return (
    <section className={styles.loadingScreen}>
      <MouseParallaxContainer>
        <MouseParallaxChild factorX={0.05} factorY={0.05}>
          <img
            quality={100}
            alt={"loading"}
            src={mask}
            className={styles.mask}
          />
        </MouseParallaxChild>
      </MouseParallaxContainer>

      <div className={styles.allProgress}>
        <progress
          style={{
            width: "100%",
          }}
          id="file"
          max="100"
          value={percent}
        />

        <div className={styles.loadingTxt}>
          <p className={styles.loadingNumber}>
            <img src={arrows} quality={100} alt={"ratio loaded"} />
            LOADING - {percent}%
          </p>
          <p className={styles.loadingUrl}>
            HTTPS://SILVERWOLF.COM/<span>{urlTxt}</span>
          </p>
        </div>
      </div>
    </section>
  );
};

export default LoadScreen;
