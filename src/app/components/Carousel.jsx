import { React } from "react";

import styles from "../styles/whoami.module.css";

import cross from "../../images/cross.png";
import ellipse1 from "../../images/ellipse.png";
import buttons from "../../images/buttons.png";
import Marquee from "react-fast-marquee";

const scrollTxtContainer = () => {
  return (
    <div className={styles.scrollTxtContainer}>
      <span>BANNED PLAYER</span>
      <img src={cross} width={60} quality={100} alt="cross" />
      <span>STELLARON HUNTER</span>
      <img src={ellipse1} width={60} quality={100} alt="ellipse" />
      <span>PUNKLORDIAN</span>
      <img src={buttons} width={60} quality={100} alt="buttons" />
    </div>
  );
};

const Carousel = () => {
  return (
    <div className={styles.scroll_wrapper}>
      <Marquee
        style={{ overflow: "visible" }}
        className={styles.scroll_inner}
        children={scrollTxtContainer()}
        // autoFill={"true"}
      ></Marquee>
    </div>
  );
};

export default Carousel;
