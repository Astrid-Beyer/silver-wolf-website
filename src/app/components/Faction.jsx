import React from "react";
import styles from "../styles/faction.module.css";

import stellaronHunter from "../../images/stellaronHunter.png";
import sw from "../../images/icon_sw.png";
import s from "../../images/icon_s.png";
import kfk from "../../images/icon_kfk.png";

const Faction = () => {
  return (
    <section>
      <h2 id="faction">Faction</h2>
      <div className={styles.faction}>
        <img
          src={stellaronHunter}
          alt={"stellaron hunter logo"}
          className={styles.stellaronLogo}
        />
        <p>We collect stellarons</p>
      </div>

      <div className={styles.team}>
        <div className={styles.elements}>
          <img src={sw} quality={100} alt={"sw icon"} />
          <p>Yup, that's me.</p>
        </div>
        <div className={styles.elements}>
          <img src={s} quality={100} alt={"s icon"} />
          <p>
            He said he'd game with me once his hand healed up, but it seems like
            it still isn't any better.
          </p>
        </div>
        <div className={styles.elements}>
          <img src={kfk} quality={100} alt={"kfk icon"} />
          <p>Ignoring the rules is something Kafka and I have in common.</p>
        </div>
      </div>
    </section>
  );
};

export default Faction;
