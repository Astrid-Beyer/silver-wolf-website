import React from "react";
import styles from "../styles/main.module.css";

import splashArt from "../../images/splashArt_sw.png";
import sticker from "../..//images/sticker.png";

import { useLenis } from "@studio-freight/react-lenis";

const MainSection = () => {
  const lenis = useLenis(({ scroll }) => {});
  const scrollToWAI = () => {
    lenis.scrollTo("#whoami");
  };
  const scrollToAE = () => {
    lenis.scrollTo("#aetherediting");
  };
  const scrollToFaction = () => {
    lenis.scrollTo("#faction");
  };

  return (
    <section id={"mainSection"}>
      <menu className={styles.description}>
        <ul>
          <li onClick={scrollToWAI} className={styles.navMenu}>
            WHO AM I
          </li>

          <li onClick={scrollToAE} className={styles.navMenu}>
            AETHER EDITING
          </li>

          <li onClick={scrollToFaction} className={styles.navMenu}>
            FACTION
          </li>
        </ul>
      </menu>

      <img
        className={styles.splashart}
        src={splashArt}
        width={950}
        alt="Silver wolf splash art"
        quality={100}
      />

      <img className={styles.sticker} src={sticker} alt="Silver wolf sticker" />

      <div className={styles.headingBlock}>
        <div className={styles.catchphrase}>
          <h1>
            Hi.
            <span className={styles.highlight}>
              Silver <br />
              Wolf <span className={styles.highlight2}>here.</span>
            </span>
          </h1>
        </div>
        <div className={styles.subline}>
          <p>Welcome to my personal space.</p>
          <p className={styles.typewriter}>Scroll down for more :)</p>
        </div>
      </div>
    </section>
  );
};

export default MainSection;
