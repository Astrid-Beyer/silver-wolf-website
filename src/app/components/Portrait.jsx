import React, { useRef } from "react";
import { useParallax, Parallax } from "react-scroll-parallax";

import styles from "../styles/portrait.module.css";

import img from "../../images/DALLE3-transformed2.jpg";

const Portrait = () => {
  const { ref } = useParallax({ speed: 10 });

  return (
    <section className={styles.portraitSection}>
      <Parallax translateY={["-500px", "500px"]}>
        <img
          ref={ref}
          className={styles.cover}
          alt="portrait"
          src={img}
          quality={100}
        />
      </Parallax>
    </section>
  );
};

export default Portrait;
