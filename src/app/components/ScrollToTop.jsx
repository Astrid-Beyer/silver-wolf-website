import { React, useState, useEffect } from "react";
import { useLenis } from "@studio-freight/react-lenis";

import styles from "../styles/page.module.css";
import scrollTopImg from "../../images/scrollToTop.png";

const ScrollToTopComponent = () => {
  const lenis = useLenis(({ scroll }) => {});

  const [showButton, setShowButton] = useState(false);

  const scrollFunction = () => {
    if (window.scrollY > (50 * window.innerHeight) / 100) {
      setShowButton(true);
    } else {
      setShowButton(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", scrollFunction);
    return () => {
      window.removeEventListener("scroll", scrollFunction);
    };
  }, []);

  const scrollUp = () => {
    lenis.scrollTo("#mainSection");
  };

  return (
    <img
      src={scrollTopImg}
      quality={100}
      width={40}
      height={40}
      className={`${styles.scrollToTop} ${showButton ? styles.show : ""}`}
      onClick={scrollUp}
    />
  );
};

export default ScrollToTopComponent;
