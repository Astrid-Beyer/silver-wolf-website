import { React } from "react";
import { SceneLoader } from "@babylonjs/core/Loading/sceneLoader";

import styles from "../styles/whoami.module.css";

import elements from "../../images/elements.png";
import {
  FreeCamera,
  Vector3,
  HemisphericLight,
  Engine,
  Matrix,
} from "@babylonjs/core";
import SceneComponent from "babylonjs-hook";
import "babylon-mmd/esm/Loader/pmxLoader";

import Carousel from "./Carousel";
import LoadScreen from "./LoadingScreen";

import emoji from "../../images/emoji.png";
import blackStar from "../../images/blackStar.png";
import whiteStar from "../../images/whiteStar.png";
import face from "../../images/face.png";

const ThreeDComponent = ({ onSceneReady }) => {
  return (
    <SceneComponent antialias onSceneReady={onSceneReady} id="my-canvas" />
  );
};

const ColoredLine = ({ color }) => (
  <hr
    style={{
      color: color,
      backgroundColor: color,
      height: 1,
      border: 0,
      margin: "0.5em 0",
    }}
  />
);

const WhoAmI = () => {
  const ParentComponent = () => {
    let scene;
    let canvas;
    let rotateFlag = true;
    let mesh;

    var lastPointerX, lastPointerY;
    function onPointerMove() {
      if (!rotateFlag) {
        var diffX = scene.pointerX - lastPointerX;
        var diffY = scene.pointerY - lastPointerY;
        mesh.rotation.y -= diffX * 0.01;
        mesh.rotation.x -= diffY * 0.01;
        lastPointerX = scene.pointerX;
        lastPointerY = scene.pointerY;
      }
    }
    function onPointerDown() {
      lastPointerX = scene.pointerX;
      lastPointerY = scene.pointerY;
      rotateFlag = false;
    }
    function onPointerUp() {
      rotateFlag = true;
    }

    const handleSceneReady = (currentScene) => {
      scene = currentScene;
      scene.autoClear = false;

      const camera = new FreeCamera("camera1", new Vector3(0, 1, -15), scene);
      camera.rotation = new Vector3(0, 15, 0);
      camera.setTarget(Vector3.Zero());

      canvas = scene.getEngine().getRenderingCanvas();

      const light = new HemisphericLight("light", new Vector3(0, 1, 0), scene);
      light.intensity = 2.3;

      const onProgress = (evt) => {
        let loadedPercent = evt.lengthComputable
          ? Math.floor((evt.loaded * 100) / evt.total)
          : Math.floor((evt.loaded / (1024 * 1024)) * 100.0) / 100.0;
        // console.log(loadedPercent);
      };

      SceneLoader.ImportMeshAsync("", "3D/", "炮.pmx", scene, onProgress).then(
        (result) => {
          const mmdMesh = result.meshes[0];
          mesh = mmdMesh;
          // get center
          const centerOfMass = mmdMesh.getBoundingInfo().boundingBox.center;
          // set origin
          const pivotMatrix = Matrix.Translation(
            -centerOfMass.x,
            -centerOfMass.y,
            -centerOfMass.z
          );
          // apply origin to center
          mmdMesh.bakeTransformIntoVertices(pivotMatrix);

          mmdMesh.rotation.x = 5.6;
          animateModel(mmdMesh);
        }
      );
      canvas.addEventListener("pointermove", onPointerMove, false);
      canvas.addEventListener("pointerdown", onPointerDown, false);
      canvas.addEventListener("pointerup", onPointerUp, false);

      scene.onDispose = function () {
        canvas.removeEventListener("pointermove", onPointerMove);
        canvas.removeEventListener("pointerdown", onPointerDown, false);
        canvas.removeEventListener("pointerup", onPointerUp, false);
      };
    };

    const animateModel = (mmdMesh) => {
      const rpm = 10;

      const renderFrame = () => {
        if (scene && rotateFlag) {
          const deltaTimeInMillis = scene.getEngine().getDeltaTime();
          mmdMesh.rotation.y +=
            (rpm / 60) * Math.PI * 2 * (deltaTimeInMillis / 1000);
          scene.render();
        }
      };

      const engine = new Engine(canvas, true);
      engine.runRenderLoop(renderFrame);
    };

    return (
      <div className={styles.threed}>
        <LoadScreen>
          <ThreeDComponent onSceneReady={handleSceneReady} />
        </LoadScreen>
      </div>
    );
  };
  return (
    <section>
      <Carousel />

      <div id="whoami">
        <h2>Who am I</h2>
        <div className={styles.whoami}>
          <div className={styles.whoamiComponents}>
            <div id="card">
              <div className={styles.tab}>
                <div className={styles.firstLine}>
                  <div className={styles.firstLineLeft}>
                    <img
                      src={face}
                      alt="faceID"
                      quality={100}
                      className={styles.face}
                    />
                  </div>
                  <div className={styles.topRight}>
                    <div className={styles.title}>
                      <img
                        src={emoji}
                        alt="emoji"
                        quality={100}
                        className={styles.emoji}
                      />
                      <h3>Silver wolf</h3>
                    </div>

                    <ColoredLine color="black" />

                    <p className={styles.whoamidesc}>
                      You wont have many infos about me. I made this for fun its
                      not supposed to be *that* serious. Anyway.
                    </p>
                  </div>
                </div>

                <ColoredLine color="black" />
                <div className={styles.bottom}>
                  <div>
                    <div className={styles.title}>
                      <img src={blackStar} alt="black star" quality={100} />
                      <h3>Hobbies</h3>
                    </div>
                    <p className={styles.desc}>
                      Gaming & hacking mostly. Also developping aether editing.
                      I like to leave graffiti behind.
                    </p>
                  </div>
                  <div>
                    <div className={styles.title}>
                      <img src={whiteStar} alt="white star" quality={100} />
                      <h3>More</h3>
                    </div>
                    <p className={styles.desc}>
                      Any account on social networks with my name are fake. You
                      wont find me anywhere else than here.
                    </p>
                  </div>
                </div>
              </div>
            </div>

            <div id="cardMobile">
              <div className={styles.tab}>
                <div className={styles.firstLine}>
                  <div className={styles.firstLineLeft}>
                    <img
                      src={face}
                      alt="faceID"
                      quality={100}
                      className={styles.face}
                    />
                  </div>
                  <div className={styles.topRight}>
                    <div className={styles.title}>
                      <img
                        src={emoji}
                        alt="emoji"
                        quality={100}
                        className={styles.emoji}
                      />
                      <h3>Silver wolf</h3>
                    </div>

                    <ColoredLine color="black" />

                    <p className={styles.whoamidesc}>
                      You wont have many infos about me. I made this for fun its
                      not supposed to be *that* serious. Anyway.
                    </p>
                  </div>
                </div>

                <ColoredLine color="black" />
                <div className={styles.bottom}>
                  <div>
                    <div className={styles.title}>
                      <img src={blackStar} alt="black star" quality={100} />
                      <h3>Hobbies</h3>
                    </div>
                    <p className={styles.desc}>
                      Gaming & hacking mostly. Also developping aether editing.
                      I like to leave graffiti behind.
                    </p>
                  </div>
                  <div>
                    <div className={styles.title}>
                      <img src={whiteStar} alt="white star" quality={100} />
                      <h3>More</h3>
                    </div>
                    <p className={styles.desc}>
                      Any account on social networks with my name are fake. You
                      wont find me anywhere else than here.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <ParentComponent />
          </div>
          <div style={{ alignSelf: "center" }}>
            <a
              href={
                "https://www.youtube.com/watch?v=8NYW1AzNLi0&ab_channel=Honkai%3AStarRail"
              }
              target="_blank"
            >
              <img src={elements} alt="elements" className={styles.elements} />
            </a>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WhoAmI;
