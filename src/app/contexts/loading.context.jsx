import { createContext, useState, useEffect } from "react";

export const loadingContext = createContext({
  percent: 0,
});

export const LoadingProvider = ({ children }) => {
  const [currentPercent, setCurrentPercent] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setCurrentPercent((prev) => (prev < 100 ? prev + 1 : 100));
    }, 25);
    return () => clearInterval(interval);
  }, []);

  return (
    <loadingContext.Provider
      value={{
        percent: currentPercent,
        setCurrentPercent,
      }}
    >
      {children}
    </loadingContext.Provider>
  );
};
